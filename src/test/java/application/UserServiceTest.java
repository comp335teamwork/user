package application;

import application.Tables.*;
import application.Repositories.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.transaction.Transactional;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = UserService.class)
@TestPropertySource(locations = "classpath:test-application.properties")
public class UserServiceTest {

    @Autowired
    WebApplicationContext wac;
    private MockMvc mockMvc;
    private String testData = "src/test/java/application/data/";

    @Autowired
    UserRepository userRepository;

    @Autowired
    SessionRepository sessionRepository;

    // Taken from https://stackoverflow.com/a/49598618/4955458
    public String readAll(String fileName) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(new File(fileName).getAbsolutePath()));
        return String.join("\n", lines.toArray(new String[lines.size()]));
    }

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    /*
    @Transactional
    @Test
    public void createAccountTest() throws Exception {
        String postBody = readAll(testData + "create-account0.json");
        String expected = "Account creation successful. Your user token is: user-";

        this.mockMvc.perform(post("/create-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(postBody))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string(org.hamcrest.Matchers.containsString(expected)));

    }


    @Transactional
    @Test
    public void loginTest0() throws Exception {
        String createAccount = readAll(testData + "create-account0.json");
        String loginData = readAll(testData + "login0.json");

        this.mockMvc.perform(post("/create-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createAccount));

        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginData))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Login successful."));

    }

    @Transactional
    @Test
    public void loginTest1() throws Exception {
        String createAccount = readAll(testData + "create-account1.json");
        String loginData = readAll(testData + "login1.json");

        this.mockMvc.perform(post("/create-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createAccount));

        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginData))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Login failed - Your password is incorrect."));

    }

    @Transactional
    @Test
    public void loginTest2() throws Exception {
        String createAccount = readAll(testData + "create-account1.json");
        String loginData = readAll(testData + "login2.json");

        this.mockMvc.perform(post("/create-account")
                .contentType(MediaType.APPLICATION_JSON)
                .content(createAccount));

        this.mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginData))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("Account not found."));

    }
    */

    @Transactional
    @Test
    public void requestUserTypeTest0() throws Exception {

        User user = new User();
        user.setUserToken("user-222df79b-4bbf-48bf-988e-35859357c14ba");
        user.setType("employee");
        userRepository.save(user);

        String userEmployee = readAll(testData + "request-user-type0.json");

        this.mockMvc.perform(post("/request-user-type")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userEmployee))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("This user is a employee."));

    }

    @Transactional
    @Test
    public void requestUserTypeTest1() throws Exception {

        User user = new User();
        user.setUserToken("user-02e5cc48-46ef-495c-9056-ed79d8a25505");
        user.setType("customer");
        userRepository.save(user);

        String userCustomer = readAll(testData + "request-user-type1.json");

        this.mockMvc.perform(post("/request-user-type")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userCustomer))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("This user is a customer."));

    }

    @Transactional
    @Test
    public void requestUserTypeTest2() throws Exception {
        String userNotFound = readAll(testData + "request-user-type2.json");

        this.mockMvc.perform(post("/request-user-type")
                .contentType(MediaType.APPLICATION_JSON)
                .content(userNotFound))
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(content().string("This user was not found."));

    }

}
