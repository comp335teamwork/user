package application;

import application.Repositories.SessionRepository;
import application.Repositories.UserRepository;
import application.Tables.Session;
import application.Tables.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.Map;
import java.util.HashMap;


@RestController
class MainController {
    private final UserRepository userRepository;
    private final SessionRepository sessionRepository;

    @Autowired
    public MainController(UserRepository userRepository, SessionRepository sessionRepository) {
        this.userRepository = userRepository;
        this.sessionRepository = sessionRepository;
    }


    //EXAMPLE
    @GetMapping(value = "/all")
    public @ResponseBody
    Iterable<User> getAllUsers() {
        System.out.println();
        return userRepository.findAll();
    }

    @PostMapping(value = "/is-admin")
    public @ResponseBody
    Map<String, Object> isAdmin(@RequestBody Map<String, Object> request) {
        String userToken = (String) request.get("user-token");
        User user = userRepository.findByUserToken(userToken);
        Map<String, Object> response = new HashMap<>();
        if (user.isAdmin()){
            response.put("isAdmin", true);
        }else {
            response.put("isAdmin", false);
        }
        return response;
    }


    //DEFAULT
    @GetMapping(path = "/")
    public @ResponseBody
    String defaultPage() {
        return "Hello World!";
    }


    /**
     * Account creation stores the user's details in the repository.
     *
     * @param user
     * @return "Account creation successful."
     */
    @PostMapping(path = "/create-account")
    public @ResponseBody
    Map<String, String> createAccount(@RequestBody final User user) {

        //userToken is set to keep track of sessions
        user.setUserToken("user-" + UUID.randomUUID().toString());

        String password = user.getPassword();
        user.setSalt(BCrypt.gensalt());
        String salt = user.getSalt();

        // hash password with a salt for security reasons
        String hashedPW = BCrypt.hashpw(password, salt);
        user.setPassword(hashedPW);

        // save user data in repository
        userRepository.save(user);

        String userToken = user.getUserToken();

        Map<String, String> creator = new HashMap<>();
        creator.put("userToken", userToken);
        creator.put("status", "success");

        return creator;
    }


    /**
     * Logging into the service begins a session for the user.
     *
     * @param userDTO
     * @return "Login successful."
     */
    @PostMapping(path = "/login")
    public @ResponseBody
    Map<String, String> login(@RequestBody final UserDTO userDTO) {

        String password = userDTO.getPassword();
        String email = userDTO.getEmail();

        //obtain the salt of the user
        User userE = userRepository.findByEmail(email);
        if (userE != null) {
            String salt = userE.getSalt();

            //hash the password
            String hashedPW = BCrypt.hashpw(password, salt);

            //check the email and hashed password against the database
            User userP = userRepository.findByPassword(hashedPW);

            if (userP != null) {
                String userToken = userP.getUserToken();

                if (userP.getPassword().equals(hashedPW)) {
                    //a session is started with the current user that has logged in
                    //this data is stored in the Session table
                    Session session = sessionRepository.findByUserToken(userToken);
                    if (session != null) {
                        sessionRepository.delete(session);
                    }
                    session = new Session(userToken);
                    sessionRepository.save(session);

                    Map<String, String> response = new HashMap<>();
                    response.put("status", "success");
                    response.put("session", session.getSessionToken());
                    return response;
                }
            } else {
                Map<String, String> response = new HashMap<>();
                response.put("status", "Incorrect password");
                return response;
            }

        }
        Map<String, String> response = new HashMap<>();
        response.put("status", "Account not found");
        return response;
    }


    /**
     * @param payment
     * @return the payment used has been accepted
     */
    @PostMapping(path = "/payment")
    public @ResponseBody
    String payment(@RequestBody String payment) {

        System.out.println("The selected payment method: " + payment);
        return "Payment details accepted.";
    }


    /**
     * @return employee data
     */
    @GetMapping(path = "/view-employees")
    public @ResponseBody
    Iterable<User> viewEmployees() {
        System.out.println();
        return userRepository.findByType("employee");
    }

    /**
     * @param userToken
     * @return the type of user
     */
    @PostMapping(path = "/request-user-type")
    public @ResponseBody
    String requestUserType(@RequestBody Map<String, Object> userToken) {

        User user = userRepository.findByUserToken(
                (String) userToken.get("userToken")
        );

        if (user != null) {
            String type = user.getType();
            return "This user is a " + type + ".";
        }

        return "This user was not found.";
    }


    /**
     * @param userToken
     * @return the sessionToken for the current user
     */
    @PostMapping(path = "/request-session-token")
    public @ResponseBody
    String requestSessionToken(@RequestBody Map<String, Object> userToken) {

        Session user = sessionRepository.findByUserToken(
                (String) userToken.get("userToken")
        );

        if (user != null)
            return "Session token: " + user.getSessionToken();

        return "This user was not found.";
    }

    /**
     * @param sessionToken
     * @return the userToken for the current session
     */
    @PostMapping(path = "/session-user")
    public @ResponseBody
    String currentUserSession(@RequestBody Map<String, Object> sessionToken) {

        Session session = sessionRepository.findBySessionToken(
                (String) sessionToken.get("sessionToken")
        );

        if (session != null) {
            return "User token: " + session.getUserToken();
        }

        return "This session was not found.";
    }

}
