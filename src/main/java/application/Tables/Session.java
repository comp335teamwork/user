package application.Tables;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Entity
public class Session {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "sessionID")
    private Integer sessionID;

    @Column(name = "sessionToken", unique = true)
    @NotNull
    private String sessionToken;

    @Column(name = "userToken", unique = true)
    @NotNull
    private String userToken;

    public Integer getSessionID() {
        return sessionID;
    }

    public void setSessionID(Integer sessionID) {
        this.sessionID = sessionID;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public Session() {}

    public Session(String userToken) {
        this.userToken = userToken;
        this.sessionToken = "session-" + UUID.randomUUID().toString();
    }

}
