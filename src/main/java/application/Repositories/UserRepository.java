package application.Repositories;

import application.Tables.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface UserRepository extends CrudRepository<User, Long> {

    List<User> findByType(String type);
    User findByUserToken(String userToken);
    User findByEmail(String email);
    User findByPassword(String password);


}
