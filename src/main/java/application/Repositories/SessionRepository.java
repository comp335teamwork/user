package application.Repositories;

import application.Tables.Session;
import org.springframework.data.repository.CrudRepository;

public interface SessionRepository extends CrudRepository<Session, Long> {

    Session findBySessionToken(String sessionToken);
    Session findByUserToken(String userToken);
}
