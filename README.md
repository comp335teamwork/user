### Setup
-[ ] Configure a dev and prod MYSQL RDBMS

-[ ] Copy `example-application.properties` into `application.properties`

-[ ] Copy `example-application.properties` into `test-application.properties`

-[ ] Fill out `application.properties` with prod credentials and `test-application.properties`
 with dev credentials.

-[ ] Run tests and all tests should pass.

